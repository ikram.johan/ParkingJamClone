using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    Rigidbody m_Rigidbody;
    [SerializeField] float moveSpeed = 3f;
    [SerializeField] float drag = 1f;
    [SerializeField] float forceMultiplier = 3f;
    [SerializeField] GameObject particleObject;
    bool m_CarDetected = false;
    bool isFollowingPath = false;
    [SerializeField] bool m_CarSelected = false;
    ParticleSystem collisionParticleSystem;
    // Start is called before the first frame update
    void Start()
    {
        if (particleObject!=null)
        {
            collisionParticleSystem = particleObject.GetComponent<ParticleSystem>();
        }
        
        m_Rigidbody = GetComponent<Rigidbody>();

        // StartCoroutine(PlayAudio());

    }
    Vector3 _lastVelocity;
    private void FixedUpdate()
    {
        if (isMoving)
        {
            Move();
        }

    }
    [SerializeField] bool isMoving = false;

    // Update is called once per frame
    void Update()
    {
        DetectCar();
        SetDirection();

    }

    bool isMovingBackward;
    public void DetectCar()
    {
        //if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
        //{
        //    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
        //    RaycastHit hit;
        //    Debug.DrawRay(ray.origin, ray.direction * 100, Color.yellow, 100f);
        //    if (Physics.Raycast(ray, out hit))
        //    {
        //        Debug.Log(hit.transform.name);
        //        if (hit.collider != null)
        //        {

        //            GameObject touchedObject = hit.transform.gameObject;

        //            Debug.Log("Touched " + touchedObject.transform.name);
        //        }
        //    }
        //}
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Debug.DrawRay(ray.origin, ray.direction * 100, Color.yellow, 100f);
            if (Physics.Raycast(ray, out hit))
            {
                Debug.Log(hit.transform.name);
                if (hit.collider != null)
                {

                    GameObject touchedObject = hit.transform.gameObject;


                    if (hit.collider.CompareTag("Car"))
                    {
                        if ((hit.collider.gameObject == this.gameObject))
                        {
                            hit.rigidbody.isKinematic = false;
                            Debug.Log("Touched " + touchedObject.transform.name);
                            m_CarDetected = true;
                            m_CarSelected = true;
                        }
                        else
                        {
                            m_CarSelected = false;
                        }
                        //isForward = true;
                    }
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            m_CarDetected = false;
        }
        if (m_CarDetected && InputManager.Instance.IsDragging)
        {
            Debug.Log("Is moving==" + isMoving);
            isMoving = true;
            StartCoroutine(PlayAudio());
            localVelocity = transform.TransformDirection(Vector3.forward).normalized;
            Debug.Log("Local velocity==" + localVelocity);
            m_CarDetected = false;

        }
    }

    bool isForward;
    private Vector3 localVelocity;

    void Move()
    {
        if (!isMoving) return;

        if (!isFollowingPath)
        {
            if (isForward)
            {
                Debug.Log("Moving car");
                m_Rigidbody.velocity = localVelocity * moveSpeed;
                Debug.Log("Velocity=" + m_Rigidbody.velocity);
            }
            else
            {
                Debug.Log("Moving car");
                m_Rigidbody.velocity = -localVelocity * moveSpeed;
                Debug.Log("Velocity= " + m_Rigidbody.velocity);
            }

        }
    }
    MoveDirection currentDirection;
    public void SetDirection()
    {
        currentDirection = InputManager.Instance.GetDragDirection();
        //Rigidbody rb = GetComponent<Rigidbody>();
        isForward = (currentDirection == MoveDirection.Up || currentDirection == MoveDirection.Right) ? true : false;
    }

    Vector3 offset;
    private void OnCollisionEnter(Collision collision)
    {
        Rigidbody rb = GetComponent<Rigidbody>();

        if (rb != null)
        {
            if (collision.collider.CompareTag("Wall") || collision.collider.CompareTag("Car"))
            {
                if (collision.collider.CompareTag("Car") && isMoving == false)
                {
                    //collision.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                    // Debug.Log("Tumi chile hridoye amar");
                    // collision.gameObject.GetComponent<Rigidbody>().constraints=RigidbodyConstraints.FreezePosition|RigidbodyConstraints.FreezeRotation;
                    // Physics.IgnoreCollision(this.gameObject.GetComponent<BoxCollider>(), collision.collider, true);
                }


                isMoving = false;
                AudioManager.Instance.StopAudio();
                AudioManager.Instance.PlayCollisionAudio();

                Debug.Log("collision detected");
                m_Rigidbody.velocity = new Vector3(0, 0, 0);

                m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY|RigidbodyConstraints.FreezePositionY;
                m_Rigidbody.drag = drag;
                collisionParticleSystem.Stop();
                collisionParticleSystem.Play();
                Invoke("StopParticle", collisionParticleSystem.main.duration);
                if (isForward)
                {
                    rb.AddForce(-localVelocity * forceMultiplier, ForceMode.Impulse);
                }
                else
                {
                    rb.AddForce(localVelocity * forceMultiplier, ForceMode.Impulse);
                }

            }
        }
    }
    
    public void StopParticle()
    {
        collisionParticleSystem.Stop(true,ParticleSystemStopBehavior.StopEmittingAndClear);
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.CompareTag("Wall") || collision.collider.CompareTag("Car"))
        {
            if (collision.collider.CompareTag("Car") && m_CarSelected)
            {
                collision.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                // collision.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                //collision.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                // Debug.Log("Tumi chile hridoye amar");
                // collision.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY|RigidbodyConstraints.FreezeRotationX|RigidbodyConstraints.FreezeRotationZ;
                //Physics.IgnoreCollision(this.gameObject.GetComponent<BoxCollider>(), collision.collider, true);
            }
            m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ|RigidbodyConstraints.FreezeRotationY|RigidbodyConstraints.FreezePositionY;
            m_Rigidbody.drag = 1f;
           
            Invoke("ResetRigidBody", 1f);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Road"))
        {
            int roadID = other.GetComponent<RoadIdentidier>().roadID;
            Debug.Log("Triggered");
            isMoving = false;
            m_Rigidbody.velocity = Vector3.zero;
            m_Rigidbody.freezeRotation = false;
            m_Rigidbody.isKinematic = true;
            // transform.Translate(new Vector3(0, 0, 0));
            GetComponent<PathFollower>().FollowPath(roadID);
            isFollowingPath = true;
            m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        }
        if (other.gameObject.CompareTag("Finish"))
        {
            VehicleManager.Instance.OnReachedFinishPoint();
            Debug.Log("Car reached to endpoint");
            AudioManager.Instance.StopAudio();
        }
    }
    IEnumerator PlayAudio()
    {

        Debug.Log("Play Audio");
        AudioManager.Instance.PlayCarDriveAudio();
        yield return new WaitWhile(() => AudioManager.Instance.audioSource.isPlaying);
        Debug.Log("Stopped");
        AudioManager.Instance.StopAudio();
    }
    void ResetRigidBody()
    {
        Debug.Log("ResetRigidBody");
        collisionParticleSystem.Play();
        m_Rigidbody.isKinematic = true;
    }
}
//transform.position+=Vector3.forward*Time.deltaTime*moveSpeed;