using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum MoveDirection
{
    Right,Left, Up,Down
}

public class InputManager : MonoBehaviour
{
    public static InputManager Instance { get; private set; }
    private MoveDirection currentMoveDirection;
    public bool IsDragging { get; set; }
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }  
    // Start is called before the first frame update
    void Start()
    {
        dragOffset = new Vector3(0.3f, 0f, 0.3f);
    }
    // Update is called once per frame
    void Update()
    {
        TouchInput();
    }
    Vector3 firstPos;
    Vector3 lastPos;
    [SerializeField] Vector3 dragOffset;
    void TouchInput()
    {
        if (Input.touchCount > 0)
        {

            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                firstPos = touch.position;
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                if (IsDragging)
                {
                    return;
                }
               // Debug.Log("touch moved");
                Vector3 pos = touch.position;
               // Debug.Log("Touch position== "+pos);
                //Vector3 posToWorld = Camera.main.ScreenToWorldPoint(pos);
               // distance = posToWorld - firstPos;
                //Debug.Log("Position= " + distance);
                lastPos=touch.position;
                Vector3 dragvector = (firstPos - lastPos).normalized;
                //  Debug.Log("Drag Vector== " + dragvector);

                IsDragging = true;
                SetDragDirection(dragvector);
                //if (Mathf.Abs(dragvector.x)>=dragOffset.x ||Mathf.Abs(dragvector.z)>=dragOffset.z)
                //{
                   
                //    Debug.Log("IS Drgging==" + IsDragging);
                   
                //}
                
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                Vector3 dragvector = (firstPos-lastPos).normalized;
               // Debug.Log("Drag Vector== "+dragvector);
                
                IsDragging = false;
            }
        }
    }
    public void SetDragDirection(Vector3 dragVector)
    {
        float positiveX = Mathf.Abs(dragVector.x);
        float positiveY = Mathf.Abs(dragVector.y);
        MoveDirection moveDirection;
        if (positiveX > positiveY)
        {
            moveDirection = (dragVector.x > 0) ? MoveDirection.Left : MoveDirection.Right;
        }
        else
        {
            moveDirection = (dragVector.y > 0) ? MoveDirection.Down : MoveDirection.Up;
        }
        Debug.Log("Move Direction=="+moveDirection);
        currentMoveDirection=  moveDirection;
    }
    public MoveDirection GetDragDirection()
    {
        return currentMoveDirection;
    }

    //public void OnEndDrag(PointerEventData eventData)
    //{
    //    Vector3 dragvector = (eventData.position - eventData.pressPosition).normalized;
    //    SetDragDirection(dragvector);
    //}
}
