using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;
    public AudioClip carAudioClip;
    public AudioClip collisionClip;

   public AudioSource audioSource;

    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayCollisionAudio()
    {
        audioSource.PlayOneShot(collisionClip);
    }
    public void PlayCarDriveAudio()
    {
        audioSource.PlayOneShot(carAudioClip);
    }

    public void StopAudio()
    {
        audioSource.Stop();
    }
}
