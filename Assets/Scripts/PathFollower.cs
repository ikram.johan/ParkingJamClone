using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFollower : MonoBehaviour
{
    public Waypoints waypoints;

    private Transform currentWaypoint;
    [SerializeField] float moveSpeed = 3f;
    [SerializeField] float distanceThreshold = 0.1f;
    [SerializeField] bool isLoop;
   [SerializeField] bool activePath;
    // Start is called before the first frame update
    void Start()
    {
        //waypoints.SetLooping(isLoop);
        //if (waypoints==null)
        //{
        //   waypoints= GameObject.FindObjectOfType<Waypoints>();
        //}
        //currentWaypoint=waypoints.GetNextWayPoint(currentWaypoint);
        //transform.position=currentWaypoint.position;
       
        //currentWaypoint = waypoints.GetNextWayPoint(currentWaypoint);
        //transform.LookAt(currentWaypoint.position);
    }

    public void FollowPath(int roadID)
    {

        if (!activePath)
        {
            waypoints.SetLooping(isLoop);
            if (waypoints == null)
            {
                waypoints = GameObject.FindObjectOfType<Waypoints>();
            }
            // currentWaypoint = waypoints.GetNextWayPoint(currentWaypoint);
            //  transform.position = currentWaypoint.position;

            currentWaypoint = waypoints.GetNextWayPoint(currentWaypoint, roadID-1);
            transform.LookAt(currentWaypoint.position);
            Debug.Log("Current Waypoint== " + currentWaypoint.gameObject.name);
            activePath = true;
        }
        
       
    }
    // Update is called once per frame
    void Update()
    {
        if (!activePath) return;
        transform.position = Vector3.MoveTowards(transform.position, currentWaypoint.position, moveSpeed * Time.deltaTime);
        if (Vector3.Distance(transform.position,currentWaypoint.position)<distanceThreshold)
        {
           // Debug.Log("Moving to next point");
            currentWaypoint = waypoints.GetNextWayPoint(currentWaypoint,null);
            transform.LookAt(currentWaypoint.position);
        }
    }
}
