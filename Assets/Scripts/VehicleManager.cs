using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleManager : MonoBehaviour
{

    public static VehicleManager Instance;
    int totalNumberofVehicles;
    int currentNumberofVehicles;
    private void Awake()
    {
        Instance = this;
        totalNumberofVehicles = GameObject.FindGameObjectsWithTag("Car").Length;
        currentNumberofVehicles = totalNumberofVehicles;
        Debug.Log("Total Vehicles==" + totalNumberofVehicles);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void OnReachedFinishPoint()
    {
        totalNumberofVehicles--;
        currentNumberofVehicles = totalNumberofVehicles;
    }

    public int GetCurrentVehicles()
    {
        return currentNumberofVehicles;
    }


}
