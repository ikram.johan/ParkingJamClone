using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [SerializeField] GameObject levelCompletedPanel;
    [SerializeField] GameObject runningGamePanel;
        // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Run());
    }  
    public void GameOver()
    {
        Debug.Log("Gameover");
        levelCompletedPanel.SetActive(true);
        runningGamePanel.SetActive(false);
    }

    IEnumerator Run()
    {
        yield return new WaitUntil(()=>(VehicleManager.Instance.GetCurrentVehicles()==0));
        GameOver();
    }
}
