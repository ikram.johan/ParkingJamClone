using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoints : MonoBehaviour
{
    [Range(0.5f, 2f)]
    [SerializeField] private float radius = 1f;
    bool loop;
    private void OnDrawGizmos()
    {
        foreach (Transform t in transform)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(t.position, radius);
        }

        Gizmos.color = Color.red;

        for (int i = 0; i < transform.childCount-1; i++)
        {
            Gizmos.DrawLine(transform.GetChild(i).position, transform.GetChild(i + 1).position);
        }
       // Gizmos.DrawLine(transform.GetChild(transform.childCount-1).position, transform.GetChild(0).position);

    }

    public void SetLooping(bool loop)
    {
        this.loop = loop;
    }
    public Transform GetNextWayPoint(Transform currentWayPoint,int? index)
    {
       
        if (currentWayPoint==null)
        {
            return transform.GetChild((int)index);
        }
        if (currentWayPoint.GetSiblingIndex()<transform.childCount-1)
        {
            return transform.GetChild(currentWayPoint.GetSiblingIndex() + 1);
        }
        else
        {
            if (!loop)
            {
                return currentWayPoint;
            }
            return transform.GetChild(0);
        }   
    }
}
